
macro_rules! parse_i {
    ($e: expr, $i: ident) => {$e.trim().parse::<$i>().unwrap()};
}

use std::io::stdin;



fn main() {
    let (m, n, map) = get_input();
//    println!("{}, {} \n{:?}", m, n, map);
    println!("{}", find_paths(&map, Coord{ x: 0, y: 0 }, m, n));
}


fn row_to_vec(s: String) -> Vec<u8> {
    let mut v = vec![];
    for n in s.chars() {
        v.push(parse_i!(format!("{}", n), u8));
    }
    v
}

fn get_input() -> (usize, usize, Vec<Vec<u8>>) {
    let mut m: String = String::new();
    stdin().read_line(&mut m);
    let m = parse_i!(m, usize);
    let mut n: String = String::new();
    stdin().read_line(&mut n);
    let n = parse_i!(n, usize);
    let mut map = vec![];
    for _ in 0..m {
        let mut input_line = String::new();
        stdin().read_line(&mut input_line).unwrap();
        let row = input_line.trim_end().to_string();
        map.push(row_to_vec(row));
    }
    (m, n, map)
}


#[derive(PartialEq, PartialOrd)]
struct Coord {
    x: usize,
    y: usize
}


impl Coord {
    fn new(x: usize, y: usize) -> Coord {
        Coord{ x, y, }
    }
    fn right(&self) -> Coord {
        Coord::new(self.x + 1, self.y)
    }
    fn down(&self) -> Coord {
        Coord::new(self.x, self.y + 1)
    }
}

fn find_paths(map: &Vec<Vec<u8>>, current_coord: Coord, m: usize, n: usize) -> usize {
    let mut possible_next = vec![];
    if current_coord == Coord::new(n-1, m-1) {
        return 1;
    }
    if current_coord.right().x < n && map[current_coord.right().y][current_coord.right().x] == 0 {
        possible_next.push(current_coord.right());
    }
    if current_coord.down().y < m && map[current_coord.down().y][current_coord.down().x] == 0 {
        possible_next.push(current_coord.down());
    }
    let mut p = 0;
    for coord in possible_next {
        p += find_paths(&map, coord, m, n);
    }
    p
}


